# diagrams

A-Core visual documentation. GitLab does not seem optimal for previewing pdf files so it might make sense to clone the repository and use a local pdf viewer instead.

## Directory structure
* drawio -- drawio save files
* pdf -- exported pdf diagrams